API
===

Introduction
------------
Integrating with the NEXTSTEP platform requires only a few simple API calls.

* :ref:`document-intake`: Submitting new documents by making a request to the platform
* :ref:`document-return`: Receiving or retrieving completed documents and metadata

The APIs that you leverage will depend on the nature and requirements of your integration.

All public APIs exposed by the NEXTSTEP use `Representational State Transfer (REST) architecture`_. API
documentation presented here is platform and language agnostic and described simply as its HTTP request
or response components.

For tutorials demonstrating how to integrate with NEXTSTEP using a particular platform or programming language
refer to :doc:`getting-started`.

.. _Representational State Transfer (REST) architecture: https://www.tutorialspoint.com/restful/restful_introduction.htm

.. _document-intake:

Document Intake
---------------
Document Intake is the process by which new documents are submitted to the platform. Intake supports
submission of documents in the following formats:

* `Tagged Image File Format (TIFF)`_
* `Joint Photographic Experts Group Format (JPEG)`_
* `Portable Network Graphics Format (PNG)`_
* `Portable Document Format (PDF)`_

When submitting documents to the platform, callers will select an appropriate API endpoint based
on the type of system that produced the incoming document, or that accepts the set of incoming metadata
that best describes the document.

.. list-table::
   :header-rows: 1
   :widths: 30, 70

   * - Endpoint
     - Description
   * - :doc:`Fax Notify <intake/fax-notify>`
     - Accepts new documents with fax-centric metadata
   * - :doc:`Generic <intake/generic>`
     - Accepts new documents with generic key-value pair metadata

.. _Tagged Image File Format (TIFF): https://en.wikipedia.org/wiki/TIFF
.. _Joint Photographic Experts Group Format (JPEG): https://en.wikipedia.org/wiki/JPEG
.. _Portable Network Graphics Format (PNG): https://en.wikipedia.org/wiki/Portable_Network_Graphics
.. _Portable Document Format (PDF): https://en.wikipedia.org/wiki/PDF

.. toctree::
   :hidden:
   :glob:

   intake/*

.. _document-return:

Document Return
---------------
Following completion of document processing the image and metadata files for the document are ready to be
returned. Files are returned to the original calling application or system in one of the following ways:

.. list-table::
   :header-rows: 1
   :widths: 30, 70

   * - Method
     - Description
   * - :doc:`Delivery by HTTP PUSH <return/document-delivery>`
     - Receiving files by accepting a request *from* the platform
   * - :doc:`Retrieval by HTTP PULL <return/document-retrieval>`
     - Retrieving files by making a request *to* the platform

.. toctree::
   :hidden:
   :glob:

   return/*
